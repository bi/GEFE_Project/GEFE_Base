# Top Level Design Parameters

# Clocks

create_clock -name {gefe_top|ClkFeedbackI_ikp} -period 40.000000 -waveform {0.000000 20.000000} ClkFeedbackI_ikp
create_clock -name {gefe_top|Osc25Mhz_ik} -period 40.000000 -waveform {0.000000 20.000000} Osc25Mhz_ik

# False Paths Between Clocks


# False Path Constraints


# Maximum Delay Constraints


# Multicycle Constraints


# Virtual Clocks
# Output Load Constraints
# Driving Cell Constraints
# Wire Loads
# set_wire_load_mode top

# Other Constraints
