// available hyper connections - for debug and ip models
// timestamp: 1495797737


`ifndef SYN_HYPER_CONNECT
`define SYN_HYPER_CONNECT 1
module syn_hyper_connect(out) /* synthesis syn_black_box=1 syn_noprune=1 */;
parameter w = 1;
parameter tag = "xxx";
parameter dflt = 0;
parameter mustconnect = 1'b1;
output [w-1:0] out;
endmodule
`endif

module GefeTop_hyper_debug(dummy);
input dummy; /* avoid compiler error for no ports */

wire GbtxElinksDclkCg_ik_0;
syn_hyper_connect GbtxElinksDclkCg_ik_connect_0(GbtxElinksDclkCg_ik_0);
defparam GbtxElinksDclkCg_ik_connect_0.tag = "i_GefeApplication.GbtxElinksDclkCg_ik";


wire AppReset_ra_0;
syn_hyper_connect AppReset_ra_connect_0(AppReset_ra_0);
defparam AppReset_ra_connect_0.tag = "i_GefeApplication.AppReset_ra";


wire [79:0] DataGbtxElinks_qb80_0;
syn_hyper_connect DataGbtxElinks_qb80_connect_0(DataGbtxElinks_qb80_0);
defparam DataGbtxElinks_qb80_connect_0.w = 80;
defparam DataGbtxElinks_qb80_connect_0.tag = "i_GefeApplication.DataGbtxElinks_qb80";


wire [1:0] DataGbtxElinksSc_qb2_0;
syn_hyper_connect DataGbtxElinksSc_qb2_connect_0(DataGbtxElinksSc_qb2_0);
defparam DataGbtxElinksSc_qb2_connect_0.w = 2;
defparam DataGbtxElinksSc_qb2_connect_0.tag = "i_GefeApplication.DataGbtxElinksSc_qb2";


wire [7:0] ujtag_wrapper_uireg_0;
syn_hyper_connect ujtag_wrapper_uireg_connect_0(ujtag_wrapper_uireg_0);
defparam ujtag_wrapper_uireg_connect_0.w = 8;
defparam ujtag_wrapper_uireg_connect_0.tag = "ident_coreinst.comm_block_INST.jtagi.ujtag_wrapper_uireg";


wire ujtag_wrapper_urstb_0;
syn_hyper_connect ujtag_wrapper_urstb_connect_0(ujtag_wrapper_urstb_0);
defparam ujtag_wrapper_urstb_connect_0.tag = "ident_coreinst.comm_block_INST.jtagi.ujtag_wrapper_urstb";


wire ujtag_wrapper_udrupd_0;
syn_hyper_connect ujtag_wrapper_udrupd_connect_0(ujtag_wrapper_udrupd_0);
defparam ujtag_wrapper_udrupd_connect_0.tag = "ident_coreinst.comm_block_INST.jtagi.ujtag_wrapper_udrupd";


wire ujtag_wrapper_udrck_0;
syn_hyper_connect ujtag_wrapper_udrck_connect_0(ujtag_wrapper_udrck_0);
defparam ujtag_wrapper_udrck_connect_0.tag = "ident_coreinst.comm_block_INST.jtagi.ujtag_wrapper_udrck";


wire ujtag_wrapper_udrcap_0;
syn_hyper_connect ujtag_wrapper_udrcap_connect_0(ujtag_wrapper_udrcap_0);
defparam ujtag_wrapper_udrcap_connect_0.tag = "ident_coreinst.comm_block_INST.jtagi.ujtag_wrapper_udrcap";


wire ujtag_wrapper_udrsh_0;
syn_hyper_connect ujtag_wrapper_udrsh_connect_0(ujtag_wrapper_udrsh_0);
defparam ujtag_wrapper_udrsh_connect_0.tag = "ident_coreinst.comm_block_INST.jtagi.ujtag_wrapper_udrsh";


wire ujtag_wrapper_utdi_0;
syn_hyper_connect ujtag_wrapper_utdi_connect_0(ujtag_wrapper_utdi_0);
defparam ujtag_wrapper_utdi_connect_0.tag = "ident_coreinst.comm_block_INST.jtagi.ujtag_wrapper_utdi";

endmodule
