device jtagport builtin
iice new {IICE} -type regular
iice controller -iice {IICE} none
iice sampler -iice {IICE} -depth 128

signals add -iice {IICE} -silent -trigger -sample {/i_GefeApplication/AppReset_ra}\
{/i_GefeApplication/DataGbtxElinksSc_qb2}\
{/i_GefeApplication/DataGbtxElinks_qb80}
iice clock -iice {IICE} -edge positive {/i_GefeApplication/GbtxElinksDclkCg_ik}

