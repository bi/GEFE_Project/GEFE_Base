*****************************************************************
Pin Report - Date: Fri May 26 13:30:12 2017 Pinchecksum: NOT-AVAILABLE
Product: Designer
Release: v11.7
Version: 11.7.0.119
Design Name: GefeTop
Family: ProASIC3L
Die: A3PE3000L
Package: 896 FBGA
*****************************************************************

-----------------------------------------------------------------
Flash*Freeze information
        Pin name  : AH4
        Pin usage : Available as a regular pin
-----------------------------------------------------------------

Port                     |Pin  |Fixed |Function         |I/O Std     |Output Drive (mA) |Slew |Resistor Pull |Schmitt Trigger |Input Delay |Skew |Output Load (pF) |Use I/O Reg |Hot Swappable |
-------------------------|-----|------|-----------------|------------|------------------|-----|--------------|----------------|------------|-----|-----------------|------------|--------------|
atck                      AD23  Fixed  TCK
atdi                      AH27  Fixed  TDI
atdo                      AF27  Fixed  TDO
atms                      AJ28  Fixed  TMS
atrstb                    AD25  Fixed  TRST
ClkFeedbackI_ikn          P30   Yes    IO117NDB3V0       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
ClkFeedbackI_ikp          P29   Yes    GCC2/IO117PDB3V0  LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
ClkFeedbackO_okn          T25   Yes    IO122NPB3V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
ClkFeedbackO_okp          R26   Yes    IO122PPB3V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
DipSwitch_ib8[0]          A11   Yes    IO21NDB0V2        LVCMOS25     ---                ---   None           Yes              Off          ---   ---               No           Yes
ElectSerialLinkTx_o       AB16  No     IO192NDB4V4       LVCMOS25_50  12                 High  None           ---              ---          No    5                 No           No
FmcPowerGoodC2m_o         AK6   No     IO215PPB5V2       LVCMOS25_50  12                 High  None           ---              ---          No    5                 No           No
FmcTck_o                  AJ19  No     IO177PDB4V2       LVCMOS25_50  12                 High  None           ---              ---          No    5                 No           No
FmcTdo_o                  AC9   No     IO226NPB5V4       LVCMOS25_50  12                 High  None           ---              ---          No    5                 No           No
FmcTms_o                  AE17  No     IO184PDB4V3       LVCMOS25_50  12                 High  None           ---              ---          No    5                 No           No
FmcTrstL_on               G28   No     IO92NDB2V1        LVCMOS25_50  12                 High  None           ---              ---          No    5                 No           No
FpgaReset_ira             R1    Yes    GFC2/IO270PDB6V4  LVCMOS25     12                 High  None           No               Off          No    5                 No           Yes
GbtxElinksDclk_ikb3n[1]   R4    Yes    GFA0/IO273NDB6V4  LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDclk_ikb3p[1]   T4    Yes    GFA1/IO273PDB6V4  LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDin_ob40n[0]    AK10  Yes    IO201NDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[1]    AK8   Yes    IO207NDB5V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[2]    AJ12  Yes    IO203NDB5V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[3]    AH15  Yes    IO195NDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[4]    AE4   Yes    IO239NDB6V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[5]    AG14  Yes    IO199NDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[6]    AE14  Yes    IO200NDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[7]    AB5   Yes    IO241NDB6V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[8]    Y9    Yes    IO253NDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[9]    T6    Yes    IO267NDB6V4       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[10]   AF12  Yes    IO208NDB5V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[11]   AB14  Yes    IO198NDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[12]   V9    Yes    IO259NDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[13]   AC13  Yes    IO204NDB5V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[14]   AB12  Yes    IO206NDB5V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[15]   U9    Yes    IO263NDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[16]   V1    Yes    IO268NDB6V4       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[17]   U8    Yes    IO265NDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[18]   Y5    Yes    IO247NDB6V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[19]   W3    Yes    IO260NDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[20]   W4    Yes    IO252NDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[21]   W5    Yes    IO251NDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[22]   AA8   Yes    IO245NDB6V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[23]   AB3   Yes    IO244NDB6V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[24]   AA3   Yes    IO248NDB6V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[25]   W7    Yes    IO255NDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[26]   Y3    Yes    IO250NDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[27]   AA4   Yes    IO246NDB6V1       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[28]   AD3   Yes    IO240NDB6V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[29]   W2    Yes    IO262NDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[30]   AJ14  Yes    IO197NDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[31]   AC6   Yes    IO237NDB6V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[32]   AC15  Yes    IO194NDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[33]   AB1   Yes    IO256NDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[34]   AC2   Yes    IO254NDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[35]   U2    Yes    IO264NDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[36]   U5    Yes    IO258NDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[37]   W1    Yes    IO266NDB6V4       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[38]   AF15  Yes    IO196NDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40n[39]   R9    Yes    IO269NDB6V4       LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksDin_ob40p[0]    AK11  Yes    IO201PDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[1]    AK9   Yes    IO207PDB5V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[2]    AJ13  Yes    IO203PDB5V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[3]    AJ15  Yes    IO195PDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[4]    AE3   Yes    IO239PDB6V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[5]    AG15  Yes    IO199PDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[6]    AF14  Yes    IO200PDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[7]    AB4   Yes    IO241PDB6V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[8]    W9    Yes    IO253PDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[9]    T7    Yes    IO267PDB6V4       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[10]   AF13  Yes    IO208PDB5V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[11]   AB15  Yes    IO198PDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[12]   V8    Yes    IO259PDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[13]   AC14  Yes    IO204PDB5V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[14]   AB13  Yes    IO206PDB5V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[15]   T9    Yes    IO263PDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[16]   U1    Yes    IO268PDB6V4       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[17]   T8    Yes    IO265PDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[18]   Y6    Yes    IO247PDB6V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[19]   V3    Yes    IO260PDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[20]   V4    Yes    IO252PDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[21]   W6    Yes    IO251PDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[22]   Y8    Yes    IO245PDB6V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[23]   AB2   Yes    IO244PDB6V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[24]   AA2   Yes    IO248PDB6V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[25]   V7    Yes    IO255PDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[26]   Y2    Yes    IO250PDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[27]   Y4    Yes    IO246PDB6V1       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[28]   AC3   Yes    IO240PDB6V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[29]   V2    Yes    IO262PDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[30]   AK14  Yes    IO197PDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[31]   AC5   Yes    IO237PDB6V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[32]   AD15  Yes    IO194PDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[33]   AA1   Yes    IO256PDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[34]   AC1   Yes    IO254PDB6V2       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[35]   U3    Yes    IO264PDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[36]   U4    Yes    IO258PDB6V3       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[37]   Y1    Yes    IO266PDB6V4       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[38]   AE15  Yes    IO196PDB5V0       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDin_ob40p[39]   R8    Yes    IO269PDB6V4       LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksDio_ib16n[0]    L9    Yes    IO293NDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[1]    N8    Yes    IO281NDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[2]    N6    Yes    IO285NDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[3]    P9    Yes    IO277NDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[4]    P4    Yes    IO284NDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[5]    P5    Yes    IO279NDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[6]    P3    Yes    IO280NDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[7]    M3    Yes    IO292NDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[8]    P2    Yes    IO278NDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[9]    P1    Yes    IO276NDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[10]   M1    Yes    IO282NDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[11]   L1    Yes    IO286NDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[12]   K1    Yes    IO288NDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[13]   J1    Yes    IO290NDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[14]   M9    Yes    IO289NDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16n[15]   N7    Yes    IO287NDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDio_ib16p[0]    L8    Yes    IO293PDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[1]    N9    Yes    IO281PDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[2]    M6    Yes    IO285PDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[3]    P8    Yes    IO277PDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[4]    N4    Yes    IO284PDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[5]    N5    Yes    IO279PDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[6]    N3    Yes    IO280PDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[7]    M4    Yes    IO292PDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[8]    N2    Yes    IO278PDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[9]    N1    Yes    IO276PDB7V0       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[10]   M2    Yes    IO282PDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[11]   L2    Yes    IO286PDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[12]   K2    Yes    IO288PDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[13]   J2    Yes    IO290PDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[14]   M8    Yes    IO289PDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDio_ib16p[15]   M7    Yes    IO287PDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24n[16]  G4    Yes    IO297NDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[17]  L3    Yes    IO298NDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[18]  J6    Yes    IO299NDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[19]  K8    Yes    IO301NDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[20]  M5    Yes    IO283NDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[21]  L6    Yes    IO291NDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[22]  J5    Yes    IO295NDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[23]  G3    Yes    IO306NDB7V4       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[24]  H3    Yes    IO300NDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[25]  K3    Yes    IO304NDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[26]  H2    Yes    IO294NDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[27]  J3    Yes    IO302NDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[28]  A9    Yes    IO17NDB0V2        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[29]  A8    Yes    IO09NDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[30]  H9    Yes    IO08NDB0V0        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[31]  J11   Yes    IO18NDB0V2        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[32]  D10   Yes    IO11NDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[33]  F8    Yes    IO12NDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[34]  C7    Yes    IO03NDB0V0        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[35]  G10   Yes    IO10NDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[36]  D9    Yes    IO05NDB0V0        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[37]  E11   Yes    IO13NDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[38]  G11   Yes    IO16NDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24n[39]  B10   Yes    IO19NDB0V2        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksDout_ib24p[16]  F4    Yes    IO297PDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[17]  L4    Yes    IO298PDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[18]  H6    Yes    IO299PDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[19]  K7    Yes    IO301PDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[20]  L5    Yes    IO283PDB7V1       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[21]  L7    Yes    IO291PDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[22]  H5    Yes    IO295PDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[23]  F3    Yes    IO306PDB7V4       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[24]  H4    Yes    IO300PDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[25]  K4    Yes    IO304PDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[26]  H1    Yes    IO294PDB7V2       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[27]  J4    Yes    IO302PDB7V3       LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[28]  A10   Yes    IO17PDB0V2        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[29]  B8    Yes    IO09PDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[30]  H10   Yes    IO08PDB0V0        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[31]  H11   Yes    IO18PDB0V2        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[32]  D11   Yes    IO11PDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[33]  F9    Yes    IO12PDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[34]  C6    Yes    IO03PDB0V0        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[35]  F10   Yes    IO10PDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[36]  C9    Yes    IO05PDB0V0        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[37]  E12   Yes    IO13PDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[38]  F11   Yes    IO16PDB0V1        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksDout_ib24p[39]  B11   Yes    IO19PDB0V2        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxElinksScIn_on         D12   Yes    IO23NDB0V2        LVDS         24                 High  ---            ---              ---          ---   0                 No           Yes
GbtxElinksScIn_op         D13   Yes    IO23PDB0V2        LVDS         24                 High  ---            ---              ---          ---   0                 Yes          Yes
GbtxElinksScOut_in        H13   Yes    IO28NDB0V3        LVDS         ---                ---   ---            ---              Off          ---   ---               No           Yes
GbtxElinksScOut_ip        H14   Yes    IO28PDB0V3        LVDS         ---                ---   ---            ---              Off          ---   ---               Yes          Yes
GbtxReset_or              U6    Yes    IO257PPB6V2       LVCMOS25     12                 High  None           ---              ---          No    5                 No           Yes
GbtxRxDataValid_i         E4    Yes    IO305PPB7V3       LVCMOS25     ---                ---   None           No               Off          ---   ---               No           Yes
GbtxTxDataValid_o         D1    Yes    IO303PPB7V3       LVCMOS25     12                 High  None           ---              ---          No    5                 No           Yes
Leds_onb6[0]              AE1   Yes    IO242PPB6V1       LVCMOS25     12                 High  None           ---              ---          No    5                 No           Yes
Leds_onb6[1]              AF2   Yes    IO238PPB6V0       LVCMOS25     12                 High  None           ---              ---          No    5                 No           Yes
Leds_onb6[2]              AG1   Yes    IO238NPB6V0       LVCMOS25     12                 High  None           ---              ---          No    5                 No           Yes
Leds_onb6[3]              W8    Yes    IO249PPB6V1       LVCMOS25     12                 High  None           ---              ---          No    5                 No           Yes
Leds_onb6[4]              AB6   Yes    IO243NPB6V1       LVCMOS25     12                 High  None           ---              ---          No    5                 No           Yes
Leds_onb6[5]              AD2   Yes    IO242NPB6V1       LVCMOS25     12                 High  None           ---              ---          No    5                 No           Yes
LemoGpioDir_o             AD14  No     IO202PPB5V1       LVCMOS25_50  12                 High  None           ---              ---          No    5                 No           No
OptoLosReset_iran         R2    Yes    GFB1/IO274PPB7V0  LVCMOS25     ---                ---   None           Yes              Off          ---   ---               No           Yes
Osc25Mhz_ik               R7    Yes    GFB2/IO271PDB6V4  LVCMOS25     ---                ---   ---            No               Off          ---   ---               No           Yes
PushButton_i              K5    Yes    GAB2/IO308PDB7V4  LVCMOS25     ---                ---   None           Yes              Off          ---   ---               No           Yes
V1p5PowerGood_i           B9    Yes    IO15PPB0V1        LVCMOS25     ---                ---   None           No               Off          ---   ---               No           Yes
V2p5PowerGood_i           B5    Yes    IO14PPB0V1        LVCMOS25     ---                ---   None           No               Off          ---   ---               No           Yes
V3p3Inhibit_o             AD13  No     IO210PPB5V2       LVCMOS25_50  12                 High  None           ---              ---          No    5                 No           No
V3p3PowerGood_i           M26   Yes    IO95PDB2V1        LVCMOS25     ---                ---   None           No               Off          ---   ---               No           Yes
